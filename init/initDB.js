const mongo = require('../mongo');
const User = require('../models/mongo/User.js');

const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
// 连接数据库
mongo.connect();

const user1 = {
  username: 'user1',
  password: '123456',
};

const user2 = {
  username: 'user2',
  password: '1234567',
}

const  newUser1 = new User(user1);
const  newUser2 = new User(user2);

newUser1.save(function (err, newUser1) {
  if (err) {
    console.log('[!!! error !!!]');
    console.error(err);
    process.exit();
  }
  console.log('[=== create user1 successfully ===]');
  console.log(newUser1);
});

newUser2.save(function (err, newUser2) {
  if (err) {
    console.log('[!!! error !!!]');
    console.error(err);
    process.exit();
  }
  console.log('[=== create user1 successfully ===]');
  console.log(newUser2);
});

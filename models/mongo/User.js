const mongoose = require('mongoose');

const Schema = mongoose.Schema

// Schema
const UserSchema = new Schema({
    username:  { type: String, required:[true, 'username cannot be null'] },
    password: { type: String, required:[true, 'password cannot be null'] },
});

UserSchema.set('autoIndex', false);
const User = mongoose.model('User',UserSchema,'Users');

module.exports = User;
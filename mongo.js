const dbops = require('./conf/db.json');

const mongoose = require('mongoose').set('debug', false);
const options = {
    user: dbops.user,
    pass: dbops.pass,
    autoIndex: false,
    poolSize: 20, // 同时保持mongodb的最大socket连接数
    keepAlive: 120,
    autoReconnect: true,
    useNewUrlParser: true,
}

module.exports = {
    connect: ()=> {            
        mongoose.connect(dbops.uri,options)
        let db = mongoose.connection
		console.log(db);
		//console.log(db.db);
        db.on('error', () => {
            console.log('[=== mongodb conect failed ! ===]');
            process.exit();
        });
        db.once('open', ()=> {
            console.log('[=== mongodb connect suucess ===]');
        })
    }
}
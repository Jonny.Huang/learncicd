/* global require */
const supertest = require('supertest');
const assert = require('chai').assert;
const User = require('../models/mongo/User');
const app = require('../index');
const requestClient = supertest(app);

const user1 = {
  username: 'user1',
  password: '123456',
}


describe('#GET user', () => {
  // append test user
  before(async () => {
    // runs once before the first test in this block
    // clear database
    await User.remove();
    const newUser1 = new User(user1);
    const user1Saved = await newUser1.save();
  });
  it('#All users', async () => {
    const res = await requestClient.get('/user/all');
    assert.isArray(res.body);
    assert.deepStrictEqual(true, res.body.length === 1);
    const userquery = res.body[0];
    assert.hasAllKeys(userquery, [
      "_id",
      "username",
      "password",
      "__v",
    ]);
    assert.deepStrictEqual(userquery.username, user1.username);
    assert.deepStrictEqual(userquery.password, user1.password);
  })
});



/* global require */
const supertest = require('supertest');
const assert = require('chai').assert;
const User = require('../models/mongo/User');
const app = require('../index');
const requestClient = supertest(app);

const user1 = {
  username: 'user1',
  password: '123456',
}


describe('#Login', function () {
  // append test user
  before(async () => {
    // runs once before the first test in this block
    // clear database
    await User.remove();
    const newUser1 = new User(user1);
    const user1Saved = await newUser1.save();
  });
  it('Login success', async () => {
    const res = await requestClient.post('/user/login')
      .send({
        username: user1.username,
        password: user1.password,
      });
    assert.hasAllKeys(res.body, [
      "username",
      "login",
      "token",
      "timestamp"
    ]);
    assert.deepStrictEqual(res.body.login, 'success');
    assert.deepStrictEqual(res.body.username, user1.username);
    assert.deepStrictEqual(true, res.body.token.length > 0);
  });

  it('Login failed by password', async () => {
    const res = await requestClient.post('/user/login')
      .send({
        username: 'user1',
        password: '1234567',
      });
    assert.hasAllKeys(res.body, [
      "code",
      "message",
      "login"
    ]);
    assert.deepStrictEqual(res.body.login, 'failed');
    assert.deepStrictEqual(res.body.code, 'bad_password');
    assert.deepStrictEqual(res.body.message, 'wrong password');
  });

  it('Login failed by not exists', async () => {
    const res = await requestClient.post('/user/login')
      .send({
        username: 'user123',
        password: '1234567',
      });
    assert.hasAllKeys(res.body, [
      "code",
      "message",
      "login"
    ]);
    assert.deepStrictEqual(res.body.login, 'failed');
    assert.deepStrictEqual(res.body.code, 'not_exists');
    assert.deepStrictEqual(res.body.message, 'user not found in db');
  });
});


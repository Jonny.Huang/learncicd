/* global require */
const supertest = require('supertest');
const assert = require('chai').assert;
const User = require('../models/mongo/User');
const app = require('../index');
const requestClient = supertest(app);

const user1 = {
  username: 'user1',
  password: '123456',
};


describe('#User Register', () => {
  before(async () => {
    // runs once before the first test in this block
    // clear database
    await User.remove();
    const newUser1 = new User(user1);
    const user1Saved = await newUser1.save();
  });
  it('Failed by exists', async () => {
    const res = await requestClient.post('/user/register')
      .send({
        username: user1.username,
        passowrd: user1.password,
      });
    assert.hasAllKeys(res.body, [
      "code",
      "message",
      "register"
    ])
    assert.deepStrictEqual(res.body, {
      register: 'failed',
      code: 'user_exists',
      message: 'user have registered',
    })
  });

  it('Success', async () => {
    const username = 'user5';
    const res = await requestClient.post('/user/register')
      .send({
        username: username,
        password: '123456',
      });
    assert.hasAllKeys(res.body, [
      "register",
      "timestamp",
      "username"
    ]);
    assert.deepStrictEqual(res.body.register, 'success');
    assert.deepStrictEqual(res.body.username, username);
    assert.deepStrictEqual(true, res.body.timestamp > 0);
  })
});

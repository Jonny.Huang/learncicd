const express =  require('express');
const User = require('../../models/mongo/User.js');

const registerRouter = new express.Router();
registerRouter.post('/user/register', async (req, res) =>{
  // error info
  const error = {
    register: 'failed',
  };
  console.log('[=== req.body ===]')
  console.log(req.body);

  // get params
  const {
    username,
    password,
  } = req.body; 
  const queryUser = await User.findOne({username});

  //  exists
  if (queryUser) {
    error.code = "user_exists";
    error.message = "user have registered";
  }
  console.log('error');
  console.log(error);
  // send error 
  if (error.code) {
    res.send(error);
  } else {
    const newUser = new User({
      username,
      password,
    });
    const timestamp = new Date().getTime();
    const result = await newUser.save();
    if (result) {
      res.send({
        register: 'success',
        timestamp,
        username,
      });
    } else {
      res.send({
        code: 'internal_err',
        message: 'internal error',
        timestamp,
        username,
      });
    }
  }
  res.end();
});

module.exports = registerRouter;

const express =  require('express');
const User = require('../../models/mongo/User.js');

const loginRouter = new express.Router();
loginRouter.post('/user/login', async (req, res) =>{
  // error info
  const error = {
    login: 'failed',
  };
  console.log('[=== req.body ===]')
  console.log(req.body);

  // get params
  const {
    username,
    password,
  } = req.body; 
  const queryUser = await User.findOne({username});

  // not exists
  if (!queryUser) {
    error.code = "not_exists";
    error.message = "user not found in db";
  } else {
    console.log(queryUser);
    console.log(password);
    console.log(`${queryUser.password} !== ${password} > ${queryUser.password != password}`)
    if (queryUser.password != password || password === undefined || password === null) {
      error.code = "bad_password";
      error.message = "wrong password";
    }
  }
  console.log('error');
  console.log(error);
  // send error 
  if (error.code) {
    res.send(error);
  } else {
    // login success
    const timestamp = new Date().getTime();
    res.send({
      login: 'success',
      timestamp,
      username,
      token: 'login success token',
    });
  }
  res.end();
});

module.exports = loginRouter;

const express =  require('express');
const User = require('../../models/mongo/User.js');

const getAllUserRouter = new express.Router();

getAllUserRouter.get('/user/all', async (req, res) =>{
  const allusers = await User.find();
  res.send(allusers)
  res.end();
});

module.exports = getAllUserRouter;

FROM node:13.7.0-alpine3.11

WORKDIR /app

# 安装 nodemon 以实现热更新(开发环境)
#RUN npm install -g nodemon

COPY . ./

RUN npm install

EXPOSE 9997

# 可热更新运行 (开发环境)
#CMD [ "nodemon", "server.js" ]

CMD ["node", "index.js"]

var express = require('express');
const mongo = require('./mongo');
const app = express();
const loginRouter = require('./controllers/User/Login.js');
const registerRouter = require('./controllers/User/Register');
const getAllUserRouter = require('./controllers/User/getAll.js');
const bodyParser = require('body-parser')


// connect db
mongo.connect();
app.use(getAllUserRouter);


app.use(bodyParser.json())
// use routers
app.use(loginRouter);
app.use(registerRouter);



app.get('/', function(req, res) {
  res.send('hello, cicd latest');
});

module.exports =  app.listen(9997);

